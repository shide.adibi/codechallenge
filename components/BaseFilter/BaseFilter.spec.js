import {mount} from '@vue/test-utils'
import BaseFilter from "./BaseFilter.vue";

describe("Mounted BaseFilter", () => {
  const wrapper = mount(BaseFilter);

  test("is a Vue instance", () => {
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  it('renders correctly', () => {
    expect(wrapper.element).toMatchSnapshot()
  })

});

