import { mount } from '@vue/test-utils'
import PeopleLists from "./PeopleLists.vue";

describe("Mounted PeopleLists", () => {
  const wrapper = mount(PeopleLists);

  test("is a Vue instance", () => {
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  it('renders correctly', () => {
    expect(wrapper.element).toMatchSnapshot()
  })

});
