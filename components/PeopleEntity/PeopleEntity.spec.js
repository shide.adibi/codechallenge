import { mount } from '@vue/test-utils'
import PeopleEntity from "./PeopleEntity.vue"

describe("Mounted PeopleEntity", () => {
  const wrapper = mount(PeopleEntity,{
    propsData:{
      person: { hair_color:'blue', eye_color:'red',gender:'female' },
      planet: {name:'naboo' }
    }
  });

  test("is a Vue instance", () => {
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  it('renders correctly', () => {
    expect(wrapper.element).toMatchSnapshot()
  })

});
