import {mount} from '@vue/test-utils'
import BaseAvatar from './BaseAvatar.vue';

describe("Mounted BaseAvatar", () => {
  const wrapper = mount(BaseAvatar, {
    propsData: {
      person: {
        gender: 'female',
        hair_color: 'brown',
        eye_color: 'brown',
        skin_color: 'brown'
      }
    }
  });

  test("is a Vue instance", () => {
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  it('renders correctly', () => {
    expect(wrapper.element).toMatchSnapshot()
  })

});
