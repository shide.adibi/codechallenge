import {mount} from '@vue/test-utils'
import BaseChip from "./BaseChip.vue";

describe("Mounted BaseChip", () => {
  const wrapper = mount(BaseChip, {
    propsData: {
      item: 'test-item',
      title: 'test-title',
      link: 'test-link',
      icon: 'test-icon'
    }
  });

  test("is a Vue instance", () => {
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  it('renders correctly', () => {
    expect(wrapper.element).toMatchSnapshot()
  })

});
