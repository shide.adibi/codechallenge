import { shallowMount } from '@vue/test-utils'
import PlanetDetailItem from "./PlanetDetailItem.vue";

describe("Mounted PlanetDetailItem", () => {
  const wrapper = shallowMount(PlanetDetailItem, {
    propsData: {
      title: 'test-title',
      description:'test-description'
    }
  });

  test("is a Vue instance", () => {
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  it('renders correctly', () => {
    expect(wrapper.element).toMatchSnapshot()
  })

});
