# code-challenge

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```


# Screenshots

## Homepage
![1](assets/images/screen1.png)

## Filtered Result
![2](assets/images/screen3.png)

## Planet Details
![3](assets/images/screen2.png)
