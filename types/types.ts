export interface ResultData<T> {
  count: number,
  results: T[]
}

export interface PersonData {
  name: string,
  height: number,
  mass: number,
  hair_color: string,
  "skin_color": string
  "eye_color": string
  "birth_year": string,
  "gender": string,
  "homeworld": string,
  "films": string[],
}

export interface FilmData {
  "title": string,
  "episode_id": number,
  "opening_crawl": string,
  "producer": string,
  "release_date": string,
  "characters": PersonData[],
  "planets": PlanetData[],
  "starships": StarshipsData[],
  "vehicles": VehiclesData[],
  "species": SpeciesData[],
  "created": string,
  "edited": string,
  "url": string,
}

export interface PlanetData {
  "name": string,
  "rotation_period": string,
  "orbital_period": string,
  "diameter": string,
  "climate": string,
  "gravity": string,
  "terrain": string,
  "surface_water": string,
  "population": string,
  "residents": PersonData[],
  "films": FilmData [],
  "created": string,
  "edited": string,
  "url": string,
}

export interface StarshipsData {
}

export interface VehiclesData {
}

export interface SpeciesData {
}
